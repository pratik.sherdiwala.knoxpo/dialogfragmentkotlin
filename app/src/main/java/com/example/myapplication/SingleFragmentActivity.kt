package com.example.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

 abstract class SingleFragmentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(getLayoutId())

        val fragmentManager = getSupportFragmentManager()

        val existingFragment = fragmentManager.findFragmentById(R.id.fragment_container)

        if (existingFragment == null)
        {
            fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, getFragment())
                .commit()
        }
    }

    //protected fun layoutId(): Int = R.layout.activity_single_fragment

    protected open fun getLayoutId(): Int {
        return R.layout.activity_single_fragment
    }

    protected abstract fun getFragment():Fragment

}


