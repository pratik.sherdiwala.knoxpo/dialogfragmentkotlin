package com.example.myapplication

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : SingleFragmentActivity(), View.OnClickListener, MainFragment.Callback {

    override fun getFragment(): Fragment {
        var r = Random()
        var num = r.nextInt(100) + 1
        return MainFragment.newInstance(num)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        incrementIB.setOnClickListener(this)
        decrementIB.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        val mainFragment = supportFragmentManager.findFragmentById(R.id.fragment_container) as MainFragment

        when (v?.id) {
            R.id.incrementIB -> {
                mainFragment.increment()
            }

            R.id.decrementIB -> {
                mainFragment.decrement()
            }
        }
    }

    override fun showNum(num: Int) {
        shownNumTV.text = num.toString()
    }
}