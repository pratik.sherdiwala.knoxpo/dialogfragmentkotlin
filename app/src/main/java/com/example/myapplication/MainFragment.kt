package com.example.myapplication

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment() {

    companion object {
        private val TAG = MainFragment::class.java.simpleName
        var ARGS_NUM = "$TAG.ARGS_NUM"

        fun newInstance(num: Int): MainFragment {
            val args = Bundle()
            args.putInt(ARGS_NUM, num)
            val fragment = MainFragment()
            fragment.arguments = args
            return fragment
        }
    }

    interface Callback {
        fun showNum(num: Int)
    }

    private var mCallback: Callback? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_main, container, false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val number = arguments!!.getInt(ARGS_NUM)
        updateTextView(number)
        showBTN.setOnClickListener(View.OnClickListener {
            mCallback?.showNum(Integer.parseInt(numberTV.text.toString()))
        })
    }

    private fun updateTextView(num: Int) = numberTV.setText(num.toString())

    fun increment() {
        var num = Integer.parseInt(numberTV.text.toString())
        updateTextView(++num)
    }

    fun decrement() {
        var num = Integer.parseInt(numberTV.text.toString())
        updateTextView(--num)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mCallback = activity as Callback?
    }

    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }
}